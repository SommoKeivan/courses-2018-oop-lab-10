package it.unibo.oop.lab.lambda.ex02;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 */
public class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();
 
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    public Stream<String> orderedSongNames() {
        return songs.stream()
        		.map(s -> s.getSongName()) //.map(Song::getSongName
        	    .sorted();
    }

    public Stream<String> albumNames() {
        return albums.entrySet().stream()
        			 .map(s -> s.getKey());
        //PROF: return this.albums.keySet().stream();
    }
    
    public Stream<String> albumInYear(final int year) {
    	return albums.entrySet().stream()
   			 		 .filter(s -> s.getValue().equals(year))
   			 		 .map(s -> s.getKey());
    }

    public int countSongs(final String albumName) {
        return (int)songs.stream()
        				 .filter(s -> s.getAlbumName().isPresent())
        				 .filter(s -> s.getAlbumName().get().equals(albumName))
        				 .count();
    }

    public int countSongsInNoAlbum() {
		return (int)songs.stream()
						 .filter(s -> s.getAlbumName().isPresent()==false) //filter(s -> !s.getAlbumName().isPresent())
						 .count();
    }

    public OptionalDouble averageDurationOfSongs(final String albumName) {
    	return songs.stream()
    			    .filter(s -> s.getAlbumName().isPresent())
    			    .filter(s -> s.getAlbumName().get().equals(albumName))
    			    .mapToDouble(s -> s.getDuration())
    			    .average();
    } 

    public Optional<String> longestSong() {
    	return songs.stream()
    				.collect(Collectors.maxBy((s1,s2) -> Double.compare(s1.getDuration(),s2.getDuration())))
    				.map(Song::getSongName);
    }

    public Optional<String> longestAlbum() {
        return songs.stream()
        			.filter(a -> a.getAlbumName().isPresent())
        			.collect(Collectors.groupingBy(Song::getAlbumName, Collectors.summingDouble(Song::getDuration)))
        			.entrySet().stream()
        			.collect(Collectors.maxBy((s1,s2) -> Double.compare(s1.getValue(), s2.getValue())))
        			.flatMap(Entry::getKey);
    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
